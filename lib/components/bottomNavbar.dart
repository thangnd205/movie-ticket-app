import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:movie_ticket_app/components/currentBottomNavigation.dart';

class BottomNavBar extends StatelessWidget {
  // void _setCurrentIndex(BuildContext context, int index) {
  //   Provider.of<CurrentBottomNavigation>(context, listen: false).changeCurrentIndex(index);
  // }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    // var currentIndex = Provider.of<CurrentBottomNavigation>(context).getIndex();

    return BottomNavigationBar(
      // currentIndex: currentIndex,
      // onTap: (index) => _setCurrentIndex(context, index),
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'HOME',
            backgroundColor: Colors.black12),
        BottomNavigationBarItem(
            icon: Icon(Icons.local_movies_outlined),
            label: 'SHOWTIMES',
            backgroundColor: Colors.black12),
        BottomNavigationBarItem(
            icon: Icon(Icons.local_grocery_store),
            label: 'STORE',
            backgroundColor: Colors.black12),
        BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'PROFILE',
            backgroundColor: Colors.black12)
      ],
    );
  }
}
